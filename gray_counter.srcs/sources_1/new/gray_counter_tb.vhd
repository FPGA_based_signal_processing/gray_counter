----------------------------------------------------------------------------------
-- Company: Physics Department E18, TU Muenchen
-- Engineer: Dmytro Levit
-- 
-- Create Date: 25-10-2016
-- Design Name: 
-- File Name: gray_counter_tb.vhd
-- Module Name: gray_counter_tb
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Last Modified: 
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use ieee.std_logic_unsigned.all;

LIBRARY UNISIM;
USE UNISIM.vcomponents.all;


ENTITY gray_counter_tb IS
END gray_counter_tb;

ARCHITECTURE implementation OF gray_counter_tb IS

    -- component declaration
    COMPONENT gray_counter IS
        PORT
        (
            clk_i      : in std_logic;
            rst_i      : in std_logic;
            inc_i      : in std_logic;
            gray_cnt_o : out std_logic_vector(3 downto 0)
        );
    END COMPONENT gray_counter;


    signal clk      : std_logic := '0';
    -- resets gray counter to 0
    signal rst      : std_logic := '0';
    -- a pulse increments gray counter by one
    signal inc      : std_logic := '0';
    signal gray_cnt : std_logic_vector(3 downto 0) := (others => '0');

    constant clk_period : time := 5 ns;

BEGIN

    -- instantiation of the component
    gray_counter_inst : gray_counter
        port map
        (
            clk_i => clk,
            rst_i => rst,
            cnt_i => inc,
            gray_cnt_o => gray_cnt
        );
    

    clk_gen_proc : PROCESS
    BEGIN
        clk <= not clk;
        wait for clk_period / 2;
    END PROCESS clk_gen_proc;

    simulation_proc : PROCESS
    BEGIN
        -- set reset
        rst <= '1';
        wait for clk_period * 10;
        -- release reset
        rst <= '0';
        wait for clk_period * 10;

        -- synchronize process to clk signal
        wait until clk = '1';
        -- increment counter
        inc <= '1';
        wait until clk = '1';
        inc <= '0';

        -- repeat increment 10 times
        for i in 0 to 10 loop
            -- wait and increment again 
            wait for clk_period * 10;

            -- synchronize process to clk signal
            wait until clk = '1';
            -- increment counter
            inc <= '1';
            wait until clk = '1';
            inc <= '0';
        end loop;

        wait;
    END PROCESS simulation_proc;

END implementation;
